﻿using UnityEngine;
using System.Collections;

public class LaserBeamSpriteScript : MonoBehaviour {

    IEnumerator PlayDeath()
    {
        for (int i = 0; i < 5; i++)
        {
            GetComponent<SpriteRenderer>().color *= 0.6f;
            yield return null;
        }
        Destroy(gameObject);
    }
}
