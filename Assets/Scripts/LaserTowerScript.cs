﻿using UnityEngine;
using System.Collections;

public class LaserTowerScript : MonoBehaviour {

    [HideInInspector]
    public TileScript locatedTile = null;
	
    public int cooldown = 300;
    private int remainingCooldown = -1;

	private int rotCooldown = 500;
	private int rotRemainingCooldown = -1;

    [SerializeField]
    private int price;

    public int Price { get { return price; } }

	public GameObject LaserSequence;

	public GameObject CooldownImage1;
	public GameObject CooldownImage2;
	public GameObject CooldownImage3;
	public GameObject CooldownFrameImage;

	public bool isRotate = false;

	// Use this for initialization
	void Start () {
		rotCooldown = cooldown * 2;
		CooldownImage1.SetActive(false);
		CooldownImage2.SetActive(false);
		CooldownImage3.SetActive(false);
		CooldownFrameImage.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(remainingCooldown > 0) {
			remainingCooldown -= 1;
			if(remainingCooldown >= cooldown / 2) {
				CooldownImage2.transform.Rotate(0, 0, -1 * 360.0f / cooldown);
				if(remainingCooldown == cooldown / 2) {
					CooldownImage3.SetActive(true);
					CooldownImage3.transform.rotation = Quaternion.Euler(0, 0, 180.0f);
				}
			} else {
				CooldownImage3.transform.Rotate(0, 0, -1 * 360.0f / cooldown);
			}
		}else if(remainingCooldown == 0) {
			remainingCooldown = -1;
			CooldownImage1.SetActive(false);
			CooldownImage2.SetActive(false);
			CooldownImage3.SetActive(false);
			CooldownFrameImage.SetActive(false);
		}

		if(rotRemainingCooldown > 0) {
			rotRemainingCooldown -= 1;
			if(rotRemainingCooldown >= rotCooldown / 2) {
				CooldownImage2.transform.Rotate(0, 0, -1 * 360.0f / rotCooldown);
				if(rotRemainingCooldown == rotCooldown / 2) {
					CooldownImage3.SetActive(true);
					CooldownImage3.transform.rotation = Quaternion.Euler(0, 0, 180.0f);
				}
			} else {
				CooldownImage3.transform.Rotate(0, 0, -1 * 360.0f / rotCooldown);
			}
		} else if(rotRemainingCooldown == 0) {
			rotRemainingCooldown = -1;
			CooldownImage1.SetActive(false);
			CooldownImage2.SetActive(false);
			CooldownImage3.SetActive(false);
			CooldownFrameImage.SetActive(false);
		}

		Vector2 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
		if(Input.GetMouseButtonDown(0) && isRotate && mousePos.x <= 6 && mousePos.x >= -6 && mousePos.y <= 4.5 && mousePos.y >= -4.5) {
			transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(mousePos.y - transform.position.y, mousePos.x - transform.position.x) / Mathf.PI * 180.0f);
			CooldownImage1.SetActive(true);
			CooldownImage2.SetActive(true);
			CooldownFrameImage.SetActive(true);
			CooldownImage1.transform.rotation = Quaternion.Euler(0, 0, 180.0f);
			CooldownImage2.transform.rotation = Quaternion.Euler(0, 0, 180.0f);
			CooldownImage1.GetComponent<SpriteRenderer>().color = new Color(0.42f, 0.14f, 0.25f, 1);
			CooldownImage3.GetComponent<SpriteRenderer>().color = new Color(0.42f, 0.14f, 0.25f, 1);
			rotRemainingCooldown = rotCooldown;
			isRotate = false;
		}
	}

	//레이저 발사
    void Fire() {

        Instantiate(LaserSequence).GetComponent<LaserBeamSequenceScript>().SetStartingPtDirection(transform.position, transform.right);
		
		CooldownImage1.SetActive(true);
		CooldownImage2.SetActive(true);
		CooldownFrameImage.SetActive(true);
		CooldownImage1.transform.rotation = Quaternion.Euler(0, 0, 180.0f);
		CooldownImage2.transform.rotation = Quaternion.Euler(0, 0, 180.0f);

		CooldownImage1.GetComponent<SpriteRenderer>().color = new Color(0.17f, 0.35f, 0.52f, 1);
		CooldownImage3.GetComponent<SpriteRenderer>().color = new Color(0.17f, 0.35f, 0.52f, 1);

		remainingCooldown = cooldown;
    }

	//마우스클릭 인식
	void OnMouseDown() {
		if(remainingCooldown == -1 && rotRemainingCooldown == -1 && !isRotate) {
			Fire();
		}
	}

	void OnMouseOver() {
		if(Input.GetMouseButtonDown(1)) {
			if(TotalManagerScript.Instance.selected != null) {
				TotalManagerScript.Instance.selected.isRotate = false;
			}
            TotalManagerScript.Instance.SelectTower(this);
			if(remainingCooldown == -1 && rotRemainingCooldown == -1) {
				if(!isRotate) {
					isRotate = true;
				}
			}
		}
	}
}
