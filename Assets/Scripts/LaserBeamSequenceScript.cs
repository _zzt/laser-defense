﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaserBeamSequenceScript : MonoBehaviour {

    [SerializeField]
    public LaserBeamScript laserType;


    [SerializeField]
    public int lifespan;

    private int remainingLifetime;

    
    public Vector2 initialStartingPoint
    { get; private set; }
    
    public Vector2 initialDirection
    { get; private set; }

    public void SetStartingPtDirection(Vector2 startingPoint, Vector2 direction)
    {
        initialDirection = direction;
        initialStartingPoint = startingPoint;
    }

    List<LaserBeamScript> lasersList = new List<LaserBeamScript>();

    // Use this for initialization
    void Start () {
        remainingLifetime = lifespan;
    }
	
	// Update is called once per frame
	void Update () {

        foreach (LaserBeamScript laser in lasersList)
        {
            laser.gameObject.SetActive(false);
        }

        NextBeam().Shoot(initialStartingPoint, initialDirection);

        remainingLifetime--;
        if (remainingLifetime <= 0)
        {
            foreach (Transform child in transform)
            {
                child.GetComponentInChildren<LaserBeamSpriteScript>().StartCoroutine("PlayDeath");
                child.DetachChildren();
                Destroy(child.gameObject);
            }
            Destroy(gameObject);
        }

    }

    

    public LaserBeamScript NextBeam(/*Vector2 startingPoint, Vector2 direction*/)
    {
        foreach (LaserBeamScript laser in lasersList)
        {
            if (!laser.gameObject.activeInHierarchy)
            {
                laser.gameObject.SetActive(true);
                return laser;
            }
        }
        LaserBeamScript newLaser = Instantiate(laserType.gameObject).GetComponent<LaserBeamScript>();
        lasersList.Add(newLaser);
        newLaser.Sequence = this;
        newLaser.transform.SetParent(transform);
        return newLaser;
    }
}
