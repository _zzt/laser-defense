﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LaserBeamScript : MonoBehaviour
{

    [SerializeField]
    private int damage;

    [HideInInspector]
    public LaserBeamSequenceScript Sequence;

    Action updateAction;

    public int Damage
    {
        get { return damage; }
        //만약 시간이 남으면 아래를 set으로 고칠 수 있게 하지 않고 다른 방법을 쓸 테지만...
        set { damage = value; }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        /*
        List<EnemyScript> grave = new List<EnemyScript>();
        foreach (EnemyScript enemy in collidedEnemies)
        {
            if (enemy == null || enemy.gameObject == null) grave.Add(enemy);
            else
                enemy.DealDamage(Damage);
        }
        foreach (EnemyScript corpse in grave)
            collidedEnemies.Remove(corpse);

        */
        /*
        updateAction();
        updateAction = delegate { };
        */
    }



    public void Shoot(Vector2 startingPoint, Vector2 direction)
    {

        int layerMask = LayerMask.GetMask(new string[] { "Consumes Lasers" });
        RaycastHit2D rh2d = Physics2D.Raycast(startingPoint, direction, float.PositiveInfinity, layerMask);
        transform.position = startingPoint;
        transform.rotation = Quaternion.Euler(0, 0, Mathf.Sign(Vector3.Cross(Vector2.right, direction).z) * Vector2.Angle(Vector2.right, direction));
        if (rh2d.collider == null)
        {

            transform.localScale = new Vector3(DistToWall(transform.position.x, transform.position.y, transform.rotation.eulerAngles.z), transform.localScale.y, 1);
            /*
            GameObject laser = GameObject.Instantiate(Laser, new Vector3(this.transform.position.x, this.transform.position.y, 0), Quaternion.identity) as GameObject;
            laser.transform.Rotate(new Vector3(0, 0, transform.rotation.eulerAngles.z));
            laser.transform.localScale = new Vector3(DistToWall(transform.position.x, transform.position.y, transform.rotation.eulerAngles.z), laser.transform.localScale.y, 1);
            */
        }
        else
        {
            
            transform.localScale = new Vector3(Vector2.Distance(transform.position, rh2d.transform.position), transform.localScale.y, 1);
            ((ILaserConsumer)rh2d.transform.GetComponent(typeof(ILaserConsumer))).ConsumeLaser(this);
        }

    }

    //List<EnemyScript> collidedEnemies = new List<EnemyScript>();
    /*
    void OnTriggerEnter2D(Collider2D other)
    {
        EnemyScript enemy = other.GetComponent<EnemyScript>();
        if (enemy != null && !collidedEnemies.Contains(enemy))
        {
            collidedEnemies.Add(enemy);
            Debug.Log("DFSADF");
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        EnemyScript enemy = other.GetComponent<EnemyScript>();
        if (enemy != null) collidedEnemies.Remove(enemy);

    }*/

    void OnTriggerEnter2D(Collider2D other)
    {
        OnTriggerStay2D(other);
    }

    void OnTriggerStay2D(Collider2D other)
    {

        EnemyScript enemy = other.GetComponent<EnemyScript>();
        if (enemy != null)
        {
            enemy.DealDamage(damage);
        }
    }

    public static float DistToWall(float x, float y, float angle)
    {

        float width = TotalManagerScript.TilesWidth / 2.0f;
        float height = TotalManagerScript.TilesHeight / 2.0f;
        switch ((int)angle)
        {
            case 0:
                return width - x;
            case 90:
                return height - y;
            case 180:
                return x + width;
            case 270:
                return y + height;
        }
        if (angle > 90 && angle < 180)
        {
            angle = 180.0f - angle;
            x *= -1;
        }
        else if (angle > 180 && angle < 270)
        {
            angle -= 180.0f;
            x *= -1;
            y *= -1;
        }
        else if (angle > 270 && angle < 360)
        {
            angle = 360.0f - angle;
            y *= -1;
        }
        angle *= Mathf.PI / 180.0f;
        if (angle <= Mathf.Acos((width - x) / Mathf.Sqrt((width - x) * (width - x) + (height - y) * (height - y))))
        {
            return 1.0f / Mathf.Cos(angle) * (width - x);
        }
        else
        {
            angle = Mathf.PI / 2.0f - angle;
            return 1.0f / Mathf.Cos(angle) * (height - y);
        }
    }
}
