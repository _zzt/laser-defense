﻿using UnityEngine;
using System.Collections;

public class EndTileScript : TileScript {

    void OnTriggerEnter2D(Collider2D other)
    {
        
        EnemyScript enemy = other.GetComponent<EnemyScript>();
        if (enemy != null)
        {
            enemy.Finish();
        }
    }

}
