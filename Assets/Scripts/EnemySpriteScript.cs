﻿using UnityEngine;
using System.Collections;

public class EnemySpriteScript : MonoBehaviour {

    IEnumerator PlayDeath()
    {
        for (int i = 0; i < 50; i++)
        {
            Color lastColor = GetComponentInChildren<SpriteRenderer>().color;
            GetComponentInChildren<SpriteRenderer>().color = new Color(lastColor.r * 0.95f, lastColor.g * 0.95f, lastColor.b * 0.95f);
            yield return null;
        }

        for (int i = 0; i < 30; i++)
        {
            //Color lastColor = GetComponentInChildren<SpriteRenderer>().color;
            GetComponentInChildren<SpriteRenderer>().color *= 0.1f;
            yield return null;
        }
        DestroyObject(gameObject);
    }

    IEnumerator PlayFinish()
    {
        for (int i = 0; i < 50; i++)
        {
            Color lastColor = GetComponentInChildren<SpriteRenderer>().color;
            GetComponentInChildren<SpriteRenderer>().color = new Color(lastColor.r * 0.95f + 0.04f, lastColor.g * 0.95f, lastColor.b * 0.95f);
            yield return null;
        }

        for (int i = 0; i < 30; i++)
        {
            //Color lastColor = GetComponentInChildren<SpriteRenderer>().color;
            GetComponentInChildren<SpriteRenderer>().color *= 0.1f;
            yield return null;
        }
        DestroyObject(gameObject);
    }
}
