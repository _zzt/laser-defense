﻿using UnityEngine;
using System.Collections;
using System;

public class IlluminatiScript : EnemyScript, ILaserConsumer {

    public override bool isBoss() { return true; }

    int scalefactor = 0;
    protected override void Update()
    {
        scalefactor++;
        this.transform.Rotate(0, 0, 1);
        if (scalefactor < 2000)
            this.transform.localScale = Vector3.one * (1+0.006f*  scalefactor);
    }

    public void ConsumeLaser(LaserBeamScript laser)
    {
        //Do nothing
    }
}
