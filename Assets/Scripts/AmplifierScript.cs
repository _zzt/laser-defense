﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AmplifierScript : MonoBehaviour, ILaserConsumer
{
	public bool isRotate = false;

	[HideInInspector]
    HashSet<LaserBeamSequenceScript> seqsMet = new HashSet<LaserBeamSequenceScript>();

    [SerializeField]
    float multiplier = 1;

    public void ConsumeLaser(LaserBeamScript laser)
    {
        if (seqsMet.Contains(laser.Sequence)) return;
        seqsMet.Add(laser.Sequence);
        LaserBeamScript next = laser.Sequence.NextBeam();
        next.Damage = (int)(laser.Damage * multiplier + 0.5f);
        next.transform.localScale = new Vector3(laser.transform.localScale.x, (0.5f * multiplier + 0.5f) * laser.transform.localScale.y, laser.transform.localScale.z);

        next.Shoot(transform.position, transform.right);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        seqsMet.Clear();

		Vector2 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
		if(Input.GetMouseButtonDown(0) && isRotate && mousePos.x <= 6 && mousePos.x >= -6 && mousePos.y <= 4.5 && mousePos.y >= -4.5) {
			transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(mousePos.y - transform.position.y, mousePos.x - transform.position.x) / Mathf.PI * 180.0f);
			TotalManagerScript.Instance.Money -= TotalManagerScript.Instance.AmpPrice / 2;
			isRotate = false;
		}
	}

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
			if(TotalManagerScript.Instance.SelectedAmp != null && TotalManagerScript.Instance.SelectedAmp.isRotate) 
			{
				TotalManagerScript.Instance.SelectedAmp.isRotate = false;
			}
			TotalManagerScript.Instance.SelectAmp(this);
			if(TotalManagerScript.Instance.Money >= TotalManagerScript.Instance.AmpPrice / 2) {
				//TotalManagerScript.Instance.SelectAmp(this);
				isRotate = true;
			}else {
				//TotalManagerScript.Instance.SelectAmp(null);
			}
        }
    }
}