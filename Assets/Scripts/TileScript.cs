﻿using UnityEngine;
using System.Collections;

public class TileScript : MonoBehaviour {

    public enum TileTypes
    {
        Road,
        Grass,
    }

    public TileTypes TileType = TileTypes.Grass;

    public void OnMouseDown()
    {
        TotalManagerScript.Instance.TileClicked(this);
    }
}
