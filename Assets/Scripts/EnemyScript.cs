﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

    [SerializeField]
    private int hp;

    [SerializeField]
    private int price;

    public virtual bool isBoss() { return false; }

    public int HP
    {
        get { return hp; }
        private set
        {
            hp = value;
            if (HP <= 0) Die();
        }
    }

    [SerializeField]
    private float baseSpeed;
    private float ranspeed;
    private int roundno;
    public void Finish()
    {
        if (GetComponentInChildren<EnemySpriteScript>() != null)
            GetComponentInChildren<EnemySpriteScript>().StartCoroutine("PlayFinish");
        // Deduce 1 life;
        TotalManagerScript.Instance.Life -= isBoss() ? 9 : 1;
        transform.DetachChildren();
        Destroy(gameObject);
		if(TotalManagerScript.Instance.Life <= 0)
		{
			Application.Quit();
		}
    }

    public void Die()
    {
        TotalManagerScript.Instance.Money += price;
        if (GetComponentInChildren<EnemySpriteScript>() != null)
            GetComponentInChildren<EnemySpriteScript>().StartCoroutine("PlayDeath");
        transform.DetachChildren();
        Destroy(gameObject);
    }

    public float Speed
    {
        get
        {
              return baseSpeed;
        //   return baseSpeed*Random.Range(0.2f, 2.2f);
        }
    }

    public void DealDamage(int amount)
    {
        HP -= amount;
    }

    public void Heal(int amount)
    {
        HP += amount;
    }
    

	// Use this for initialization
	void Start ()
    {
        ranspeed = Random.Range(0.9f, 1.1f);
        HP = HP + TotalManagerScript.Instance.Round * TotalManagerScript.Instance.Round * HP / 10;
        baseSpeed = baseSpeed *3* (1.0f + TotalManagerScript.Instance.Round * 0.025f);
        //       ranspeed = 5;
        price = 5 + TotalManagerScript.Instance.Round;
    }

    // Update is called once per frame
    protected virtual void Update()
    {

        ////////THIS IS HARDCODIIIIIIIIIIIING


        if (this.transform.position.x >= 3.3f && this.transform.position.y > 2.5 &&
            (this.transform.eulerAngles.z > 270 || this.transform.eulerAngles.z == 0))
        {
            //          Debug.Log(this.transform.rotation.z);
            this.transform.Rotate(0, 0, -2);
            this.transform.Translate(1 * 0.004f, 0, 0);
            //         Debug.Log(this.transform.eulerAngles.z);

        }
        else if (this.transform.position.x >= 3.3f && this.transform.position.y < 0.2f && this.transform.position.y > -0.2f &&
            this.transform.eulerAngles.z <= 270 && this.transform.eulerAngles.z > 180)
        {
            this.transform.Rotate(0, 0, -2);
            this.transform.Translate(1 * 0.004f, 0, 0);
        }
        else if (this.transform.position.x <= -3.3f && this.transform.position.y < 0.2f &&
                this.transform.position.y > -0.7f &&
                 this.transform.eulerAngles.z < 270)
        {
            this.transform.Rotate(0, 0, 2);
            this.transform.Translate(1 * 0.004f, 0, 0);
        }
        else if (this.transform.position.x <= -3.3f && this.transform.position.y < -2.8f &&
             this.transform.eulerAngles.z < 360)
        {
            this.transform.Rotate(0, 0, 2);
            this.transform.Translate(1 * 0.004f, 0, 0);
        }
        else if (this.transform.position.x > -2.8f && this.transform.position.y < -1.2f)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }
        else
        {
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }
        /*
        else if (this.transform.position.x >= 3.3f && this.transform.position.y > 0.2f)
        {
            Debug.Log("1st vertical line");
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }
        else if (this.transform.position.x > -3.3f && this.transform.position.y <= 0)
        {
            Debug.Log("2nd line");
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }
        else if (this.transform.position.x <= -3.3f && this.transform.position.y > -2.8
                    && this.transform.position.y < -0.1f)
        {
            Debug.Log("2nd Vertical Line");
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }
        else if (this.transform.eulerAngles.z > -1
            && this.transform.position.x <= -3 && this.transform.position.y < -2.8)
        {
            Debug.Log("HERE");
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (this.transform.position.x < 3.3f && this.transform.position.y < -3)
        {
            Debug.Log("Final");
            this.transform.Translate(Speed * ranspeed * 0.008f, 0, 0);
        }*/
    }
}
